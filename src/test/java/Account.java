import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import java.io.IOException;
import java.time.LocalDateTime;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Gunter on 5/9/2017.
 */
public class Account {

    static AndroidDriver driver = AppTest.driver;

    static LocalDateTime now = LocalDateTime.now();
    static int month = now.getMonthValue();
    static int day = now.getDayOfMonth();

    static RandomStringUtils randomStringUtils = new RandomStringUtils();
    static String randomString = randomStringUtils.randomAlphabetic(4);

    static String firstName = "Ivan";
    static String lastName = month + "" + day;
    static String username = firstName + lastName + "+" + randomString + "@example.com";
    static String password = "tester1!";

    static By envModal = By.id("alertTitle"); //Environment //Say Hello to the New Glooko
    static By qaRadioBtn = By.id("rdbQa");
    static By envOkBtn = By.id("button3");
    static By signUpBtn = By.xpath("//*[@text='Sign Up']");
    static By signUpFirstName = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[6]");
    static By signUpLastName = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[8]");
    static By signUpEmail = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[12]");
    static By signUpPassword = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[15]");
    static By newUserWelcomeNextBtn = By.xpath("//*[@text='Next']");
    static By newUserWelcomeNextBtn2 = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[2]//*[1]//*[3]//*[1]//*[1]//*[1]");
    static By newUserWelcomeDoneBtn = By.xpath("//*[@text='Have these already? Tap here to continue']");

    static By loginBtn = By.xpath("//*[@text='Login']");
    static By loginEmail = By.xpath("//*[@text='Email']");
    static By loginPassword = By.xpath("//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[1]//*[4]");
    static By firstLoginOkBtn = By.id("button1");


    static By profileButton = By.id("profile_button");
    static By logoutButton = By.xpath("//*[@text='LOG OUT']");
    static By confirmLogoutBtn = By.id("button1");

    public static void signUp() throws InterruptedException {

        AppTest.pause();
        if (driver.findElement(envModal).getText().equals("Environment")) {
            AppTest.clickOn(qaRadioBtn);
            AppTest.clickOn(envOkBtn);
        }

        AppTest.clickOn(signUpBtn);

        Thread.sleep(3000);
        AppTest.typeIn(signUpFirstName, firstName);
        AppTest.hideKeyboard();
        AppTest.pause();
        AppTest.typeIn(signUpLastName, lastName);
        AppTest.hideKeyboard();
        AppTest.pause();
        AppTest.typeIn(signUpEmail, username);
        AppTest.hideKeyboard();
        AppTest.pause();
        AppTest.typeIn(signUpPassword, password);
        AppTest.hideKeyboard();

        AppTest.clickOn(signUpBtn);

        try {
            AppTest.clickOnWaitedElement(newUserWelcomeNextBtn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AppTest.pause();
        AppTest.clickOn(newUserWelcomeNextBtn);
        AppTest.pause();
        AppTest.clickOn(newUserWelcomeNextBtn);
        AppTest.pause();
        AppTest.clickOn(newUserWelcomeNextBtn2);
        AppTest.pause();
        AppTest.clickOn(newUserWelcomeDoneBtn);
        AppTest.pause();

    }

    public static void login(String account) {
        if (driver.findElement(envModal).getText().equals("Environment")) {
            AppTest.clickOn(qaRadioBtn);
            AppTest.clickOn(envOkBtn);
        }

        AppTest.clickOn(loginBtn);
        AppTest.typeIn(loginEmail, username);
        AppTest.typeIn(loginPassword, password);

        AppTest.clickOn(loginBtn);

        AppTest.clickOnWaitedElement(firstLoginOkBtn);
    }

    public static void logout() throws InterruptedException, IOException {
        AppTest.clickOnWaitedElement(profileButton);
        AppTest.pause();
        AppTest.swipeToBottom(0.9, 300);
        AppTest.clickOnWaitedElement(logoutButton);
        AppTest.clickOn(confirmLogoutBtn);

        if (!AppTest.noElement(confirmLogoutBtn))
            AppTest.clickOnWaitedElement(confirmLogoutBtn);
        AppTest.clickOnWaitedElement(envModal);
    }

}
