import gnu.io.SerialPort;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.sound.sampled.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by sullivan on 27/04/17.
 */
public class AppTest {

    static AndroidDriver driver;

    By syncButton = By.id("toolbar_sync_button");
    By addDeviceBtn = By.id("mniAddDevice");
    By metersBtn = By.xpath("//*[@text='Meters']");

    By checkTestMeter = By.xpath("//*[@text='Test Meter']");
    By checkFreeStyleLite = By.xpath("//*[@text='Lite']");
    By checkOneTouchUltra2 = By.xpath("//*[@text='Ultra2']");
    By checkBayerContourNextUsb = By.xpath("//*[@text='CONTOUR NEXT USB']");
    By checkIsensCaresensN = By.xpath("//*[@text='CareSens N']");
    By checkFreestyleInsulinx = By.xpath("//*[@text='Insulinx']");
    By metersDoneBtn = By.xpath("//*[@text='Done']");

    By syncFreestyleLite = By.xpath("//*[@text='FreeStyle Lite']");
    By syncOneTouchUltra2 = By.xpath("//*[@text='OneTouch Ultra2']");
    By syncIsensCaresensN = By.xpath("//*[@text='i-SENS CareSens N']");
    By syncFreestyleInsulinx = By.xpath("//*[@text='FreeStyle Insulinx']");
    By syncBayerContourNextUsb = By.xpath("//*[@text='Bayer CONTOUR NEXT USB']");
    By syncTestMeter = By.xpath("//*[@text='Test Meters Test Meter']");
    By meterSyncBlue = By.id("lblHeading1");
    By meterSyncCable = By.id("lblHeading2");
    By tutorialActionBtn = By.id("action_button");

    By testSyncBtn = By.id("button3");

    By profileButton = By.id("profile_button");
    By logoutButton = By.xpath("//*[@text='LOG OUT']");
    By confirmLogoutBtn = By.id("button1");


    @BeforeClass
    public void setUp() throws IOException
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("BROWSER_NAME", "Android");
        capabilities.setCapability("VERSION", "5.0");
        capabilities.setCapability("deviceName", "Xiaomi");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("newCommandTimeout", "0");

        capabilities.setCapability("appPackage", "com.glooko.logbook");
        capabilities.setCapability("appActivity", "com.glooko.logbook.activity.FtueActivity");
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    }

    @Test
    public void PairAndSyncSetup() throws InterruptedException, IOException, LineUnavailableException, UnsupportedAudioFileException {

        Account.signUp();

        clickOnWaitedElement(syncButton);

        findMeter(checkOneTouchUltra2);
        findMeter(checkBayerContourNextUsb);
//        findMeter(checkIsensCaresensN);
//        findMeter(checkFreestyleInsulinx);

        SerialPort port = Arduino.setUpPort();

        Arduino.pairMsb();

        Arduino.syncMsbMeter(syncOneTouchUltra2, "MSB2", port);
        Arduino.syncUsbMeter(syncBayerContourNextUsb, "USB3", port);
//        Arduino.syncUsbMeter(syncFreestyleInsulinx, "USB1", port);
//        Arduino.syncMsbMeter(syncIsensCaresensN, "MSB1", port);

        Arduino.clearMeter();

        Account.logout();

    }

    @AfterClass
    public void tearDown()
    {
        Arduino.closeSerialPort();
        driver.quit();
    }

    void playSound() throws IOException, InterruptedException {
        // open the sound file as a Java input stream
        String file = "electric.wav";
        InputStream in = new FileInputStream(file);

        // create an audiostream from the inputstream
        AudioStream audioStream = new AudioStream(in);

        // play the audio clip with the audioplayer class
        AudioPlayer.player.start(audioStream);
        pause();
    }

    public void findMeter(By meter) throws InterruptedException {
        clickOn(addDeviceBtn);
        clickOnWaitedElement(metersBtn);
        pause();
        while (noElement(meter)) {
            swipeToBottom(0.5, 500);
        }
        clickOn(meter);
        swipeToBottom(0.9, 300);
        swipeToBottom(0.9, 300);
        clickOn(metersDoneBtn);
        pause();
    }

    public static void clickOn(By element) {
        driver.findElement(element).click();
    }
    public static void clickOn(By element, int index) {
        WebElement elem = (WebElement) driver.findElements(element).get(index);
        elem.click();
    }

    public static void typeIn(By element, String string) {
        driver.findElement(element).sendKeys(string);
    }

    public static void clickOnWaitedElement(By element) {
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public static void clickOnIf(By element) {
        if(!noElement(element)) clickOn(element);
    }

    public static void clickOnWaitedElement(By element, int waitSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public static void waitForElementToDisappear(By element, int waitSeconds) throws InterruptedException {
        int i = 0;
        while (!driver.findElements(element).isEmpty() && i < waitSeconds ) {
            i++;
            System.out.println(driver.findElements(element).isEmpty());
            Thread.sleep(1000);
        }
    }

    public static boolean noElement(By element) {
        return driver.findElements(element).isEmpty();
    }

    public static void pause() throws InterruptedException {
        Thread.sleep(1500);
    }

    public static void swipeToBottom(double percent, int duration) throws InterruptedException {
        //Get the size of screen.
        Dimension size = driver.manage().window().getSize();
        int starty = (int) (size.height * percent);
        int endy = (int) (size.height * 0.10);
        int startx = size.width / 2;

        driver.swipe(startx, starty, startx, endy, duration);
        Thread.sleep(1500);
    }

    public static void hideKeyboard() {
        try {
            driver.hideKeyboard();
        } catch (Exception e) {
            System.out.println("No keyboard to hide???");
        }
    }

}
