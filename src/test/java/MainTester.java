import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by Gunter on 4/28/2017.
 */
public class MainTester {

    public static void main(String[] args)
    {
        try{

            playSound();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void playSound() throws Exception {
        // open the sound file as a Java input stream
        String file = "electric.wav";
        InputStream in = new FileInputStream(file);

        // create an audiostream from the inputstream
        AudioStream audioStream = new AudioStream(in);

        // play the audio clip with the audioplayer class
        AudioPlayer.player.start(audioStream);
        Thread.sleep(1000);
    }


}
