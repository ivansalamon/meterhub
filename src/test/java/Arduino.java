import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import org.openqa.selenium.By;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

/**
 * Created by Gunter on 5/8/2017.
 */
public class Arduino {

    static SerialPort serialPort;
    static OutputStream outStream;

    static By syncMeterActionButton = By.id("action_button");
    static By syncMeterFirstConfirmBtn = By.id("btnIKnowHowToSync");
    static By syncMeterCableButton = By.id("btnContinue");

//    btnContinue action_button action_button

    static By syncMeterSecondConfirmBtn = By.id("btnSync");
    static By exitNewReadingsDialog = By.id("exit_dialog");
    static By newMeterDialogBtn = By.id("button1");
    static By syncProgressBar = By.id("progress");

    static By pairMSB = By.xpath("//*[@text='Glooko MeterSync Blue']");
    static By pairNextBtn = By.id("btnNext");
    static By pairFinishBtn = By.id("btnSelectMeter");

    static By rateAppDontAskAgainBtn = By.id("button2");

    static By syncButton = By.id("toolbar_sync_button");

    public static SerialPort setUpPort() throws IOException {

        try {
            // Obtain a CommPortIdentifier object for the port you want to open
            OutputStream outStream;
            Enumeration pList = CommPortIdentifier.getPortIdentifiers();

            while (pList.hasMoreElements()) {
                CommPortIdentifier cpi = (CommPortIdentifier) pList.nextElement();
                System.out.print("Port " + cpi.getName() + " ");
                if (cpi.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                    System.out.println("is a Serial Port: " + cpi);
                } else if (cpi.getPortType() == CommPortIdentifier.PORT_PARALLEL) {
                    System.out.println("is a Parallel Port: " + cpi);
                } else {
                    System.out.println("is an Unknown Port: " + cpi);
                }
            }

            CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier("COM28");

            CommPort commPort = portId.open("Meters Hub", 5000);
            serialPort = (SerialPort) commPort;

            serialPort.setSerialPortParams(
                    9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return serialPort;
    }

    public static void closeSerialPort() {
        if (serialPort != null)
            serialPort.close();
    }

    public static void syncMsbMeter(By meterElement, String meterId, SerialPort serialPort) throws IOException, InterruptedException {
        if(!AppTest.noElement(syncButton)) AppTest.clickOn(syncButton);
        AppTest.clickOnWaitedElement(meterElement);
        Arduino.clearMeter();
        AppTest.pause();
        Arduino.wakeMsb();
        AppTest.pause();
        outStream = serialPort.getOutputStream();
        Thread.sleep(1000);
        outStream.write(meterId.getBytes());
        AppTest.pause();
        AppTest.clickOn(syncMeterFirstConfirmBtn);
        AppTest.pause();
        if(!AppTest.noElement(syncMeterActionButton)) AppTest.clickOn(syncMeterActionButton);
        AppTest.clickOnWaitedElement(syncMeterSecondConfirmBtn);
        AppTest.waitForElementToDisappear(syncProgressBar, 60);
        if(!AppTest.noElement(newMeterDialogBtn)) AppTest.clickOn(newMeterDialogBtn);
        AppTest.clickOnWaitedElement(exitNewReadingsDialog);
        AppTest.pause();
        if(!AppTest.noElement(rateAppDontAskAgainBtn)) AppTest.clickOn(rateAppDontAskAgainBtn);
        Arduino.clearMeter();
        AppTest.pause();
    }

    public static void syncUsbMeter(By meterElement, String meterId, SerialPort serialPort) throws IOException, InterruptedException {
        if(!AppTest.noElement(syncButton)) AppTest.clickOn(syncButton);
        AppTest.clickOnWaitedElement(meterElement);
        Arduino.clearMeter();
        AppTest.pause();
        outStream = serialPort.getOutputStream();
        Thread.sleep(1000);
        outStream.write(meterId.getBytes());
        AppTest.pause();
        AppTest.clickOnIf(syncMeterCableButton);
        AppTest.clickOnWaitedElement(syncMeterActionButton);
        AppTest.clickOnWaitedElement(syncMeterActionButton);
        AppTest.clickOnIf(syncMeterActionButton);
        AppTest.clickOnIf(newMeterDialogBtn);
        AppTest.waitForElementToDisappear(syncProgressBar, 60);
        AppTest.clickOnIf(newMeterDialogBtn);
        AppTest.clickOnIf(exitNewReadingsDialog);
        AppTest.pause();
        AppTest.clickOnIf(rateAppDontAskAgainBtn);
        Arduino.clearMeter();
        AppTest.pause();
    }
    public static void clearMeter() throws IOException {
        outStream = serialPort.getOutputStream();
        outStream.write("CLR".getBytes());
    }

    public static void pairMsb() throws IOException, InterruptedException {
        AppTest.clickOnWaitedElement(pairMSB);
        AppTest.pause();
        Arduino.wakeMsb();
        AppTest.clickOn(pairNextBtn);
        AppTest.clickOnWaitedElement(pairFinishBtn);
        AppTest.pause();
    }

    public static void wakeMsb() throws IOException, InterruptedException {
        outStream = serialPort.getOutputStream();
        Thread.sleep(1000);
        outStream.write("MSBWKUP".getBytes());
    }
}
